﻿using UnityEngine;

/// <summary>
/// TouchSimulation designed for mock touch input with mouse (since most of <Touch> fields are read only).
/// Note: only position, deltaPosition, deltaTime and phase values are updated, all others is constants.
/// </summary>
public struct TouchSimulation
{
    #region PrivateMembers

    private float m_altitudeAngle;
    private float m_azimuthAngle;
    private Vector2 m_deltaPosition;
    private float m_deltaTime;
    private int m_fingerId;
    private float m_maximumPossiblePressure;
    private TouchPhase m_phase;
    private Vector2 m_position;
    private float m_pressure;
    private float m_radius;
    private float m_radiusVariance;
    private Vector2 m_rawPosition;
    private int m_tapCount;
    //private TouchType m_type;

    private Vector2 m_initialPoint;
    private Vector2 m_lastPosition;

    #endregion

    #region PublicProperties

    public float AltitudeAngle
    {
        get
        {
            return m_altitudeAngle;
        }

        set
        {
            m_altitudeAngle = value;
        }
    }

    public float AzimuthAngle
    {
        get
        {
            return m_azimuthAngle;
        }

        set
        {
            m_azimuthAngle = value;
        }
    }

    public Vector2 DeltaPosition
    {
        get
        {
            return m_deltaPosition;
        }

        set
        {
            m_deltaPosition = value;
        }
    }

    public float DeltaTime
    {
        get
        {
            return m_deltaTime;
        }

        set
        {
            m_deltaTime = value;
        }
    }

    public int FingerId
    {
        get
        {
            return m_fingerId;
        }

        set
        {
            m_fingerId = value;
        }
    }

    public float MaximumPossiblePressure
    {
        get
        {
            return m_maximumPossiblePressure;
        }

        set
        {
            m_maximumPossiblePressure = value;
        }
    }

    public TouchPhase Phase
    {
        get
        {
            return m_phase;
        }

        set
        {
            m_phase = value;
        }
    }

    public Vector2 Position
    {
        get
        {
            return m_position;
        }

        set
        {
            m_position = value;
        }
    }

    public float Pressure
    {
        get
        {
            return m_pressure;
        }

        set
        {
            m_pressure = value;
        }
    }

    public float Radius
    {
        get
        {
            return m_radius;
        }

        set
        {
            m_radius = value;
        }
    }

    public float RadiusVariance
    {
        get
        {
            return m_radiusVariance;
        }

        set
        {
            m_radiusVariance = value;
        }
    }

    public Vector2 RawPosition
    {
        get
        {
            return m_rawPosition;
        }

        set
        {
            m_rawPosition = value;
        }
    }

    public int TapCount
    {
        get
        {
            return m_tapCount;
        }

        set
        {
            m_tapCount = value;
        }
    }

    /*
    public TouchType Type
    {
        get
        {
            return m_type;
        }

        set
        {
            m_type = value;
        }
    }
    */

    #endregion

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="initialPosition"></param>
    public TouchSimulation(Vector2 initialPosition)
    {
        m_lastPosition = initialPosition;
        m_initialPoint = initialPosition;

        m_altitudeAngle = Mathf.PI / 2;
        m_azimuthAngle = 0.0f;
        m_deltaPosition = Vector2.zero;
        m_deltaTime = 0.0f;
        m_fingerId = 0;
        m_maximumPossiblePressure = 1.0f;
        m_phase = TouchPhase.Ended;
        m_position = initialPosition;
        m_pressure = 1.0f;
        m_radius = 1.0f;
        m_radiusVariance = 1.0f;
        m_rawPosition = initialPosition;
        m_tapCount = 1;
        //type = TouchType.Direct;  //It is brand new Unity3D feature, turned off for support previous versions.
    }

    /// <summary>
    /// Update instance of touch as frequently as you need. Dependent from Input.
    /// </summary>
    public void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            Phase = TouchPhase.Ended;
        } else
        {
            if (Phase == TouchPhase.Began)
            {
                if ((Vector2)Input.mousePosition != m_initialPoint)
                {
                    Phase = TouchPhase.Moved;
                }
            } else
            {
                if (Phase == TouchPhase.Moved)
                {
                    Change();
                }
            }
        }
    }

    /// <summary>
    /// When position changed, recalculate some values.
    /// </summary>
    private void Change()
    {
        Position = Input.mousePosition;
        DeltaTime = 0.0f;
        DeltaPosition = Position - m_lastPosition;
        m_lastPosition = Position;
    }
}
