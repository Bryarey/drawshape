﻿#define Support_Old_Versions
using UnityEngine;


public static class TouchExtension
{
    /// <summary>
    /// Converts standart Unity`s <Touch> class into TouchSimulation.
    /// </summary>
    /// <param name="touch">Touch instance</param>
    /// <returns>TouchSimulation instance</returns>
    static public TouchSimulation ToTouchSimulation(this Touch touch)
    {
        TouchSimulation newTouchSimulation = new TouchSimulation(touch.position);

        //There is a couple of brand new Unity3D Touch features, turned off for support previous versions.

#if !Support_Old_Versions
        newTouchSimulation.altitudeAngle = touch.altitudeAngle;
        newTouchSimulation.azimuthAngle = touch.azimuthAngle;
        newTouchSimulation.maximumPossiblePressure = touch.maximumPossiblePressure;
        newTouchSimulation.pressure = touch.pressure;
        newTouchSimulation.radius = touch.radius;
        newTouchSimulation.radiusVariance = touch.radiusVariance;
        newTouchSimulation.type = touch.type;
#endif
        newTouchSimulation.DeltaPosition = touch.deltaPosition;
        newTouchSimulation.DeltaTime = touch.deltaTime;
        newTouchSimulation.FingerId = touch.fingerId;
        newTouchSimulation.Phase = touch.phase;
        newTouchSimulation.Position = touch.position;
        newTouchSimulation.RawPosition = touch.rawPosition;
        newTouchSimulation.TapCount = touch.tapCount;
        
        return newTouchSimulation;
    }
}
