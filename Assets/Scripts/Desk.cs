﻿using UnityEngine;

namespace DrawShape
{
    /// <summary>
    /// The Desk is a surface on wich player draws. It checks inputs and performs drawing. There is no mesh "surface", it is just screen space.
    /// </summary>
    public class Desk : MonoBehaviour
    {
        #region PrivateMembers

        [SerializeField]
        private Drawing m_drawing = null;

        private TouchSimulation m_simulatedTouch;

        private GameObject m_trail;
		private GameObject m_target; //this is a visual point, shows drawing`s initial coordinates

        [SerializeField]
        private GameObject m_trailPrefab;

        [SerializeField]
        private GameObject m_targetPrefab;

        private Shape m_template;   //What user should draw

        private Game m_game;

        #endregion

        #region MonoBehaviourMessages
        private void Start()
        {
            m_game = Game.Find();
            m_game.OnStartRound += HandleOnStartRound;
            m_game.OnRoundDone += HandleOnRoundDone;
            m_game.OnResetDrawing += HandleOnResetDrawing;

            m_simulatedTouch = new TouchSimulation(Input.mousePosition);

            StartNewRound();
        }

        private void OnDestroy()
        {
            m_game.OnStartRound -= HandleOnStartRound;
            m_game.OnRoundDone -= HandleOnRoundDone;
            m_game.OnResetDrawing -= HandleOnResetDrawing;
        }

        private void Update()
        {
            if (m_game.IsPlaying)
            {
                UpdateMouseInput();
            }
        }

        #endregion

        #region Input

        /// <summary>
        /// Perform some actions according to user`s input
        /// </summary>
        private void UpdateMouseInput()
        {
            m_simulatedTouch.Update();

            if (Input.GetMouseButtonDown(0))
            {
                StartNewDrawing();
            }
            else
            {
                if (m_drawing != null)
                {
                    UpdateDrawing(m_simulatedTouch);
                }
            }
        }

        #endregion

        #region Drawing

        /// <summary>
        /// Create new drawing, lineDrawer, trail and target objects.
        /// </summary>
        private void StartNewDrawing()
        {
            if (Input.touchSupported && Input.touchCount > 0)
            { 
                //for touch input 
                Touch newTouch = Input.GetTouch(0);
                m_simulatedTouch = newTouch.ToTouchSimulation();
            } else
            {
                //for simulated with mouse
                m_simulatedTouch = new TouchSimulation(Input.mousePosition);
                m_simulatedTouch.Phase = TouchPhase.Began;
            }

            //create new drawing object:
            m_drawing = new Drawing(m_simulatedTouch, m_template);

            //create new trail:
            Vector3 trailPosition = Camera.main.ScreenToWorldPoint(m_simulatedTouch.Position);
            trailPosition.z = -1;
            m_trail = Instantiate(m_trailPrefab, trailPosition, Quaternion.identity) as GameObject;

            //Trail Slave - is a line drawer.
            // Set trail slave time to round length - trail should be there for the end of round:
            GameObject trailSlave = m_trail.transform.GetChild(0).gameObject;    //locate trailSlave
            trailSlave.GetComponent<TrailRenderer>().time = Game.Find().RoundLength;    //set trail slave time

            m_target = Instantiate(m_targetPrefab, trailPosition, Quaternion.identity) as GameObject;
        }

        /// <summary>
        /// Update current drawing with new input
        /// </summary>
        /// <param name="_simulatedTouch"> new input </param>
        private void UpdateDrawing(TouchSimulation _simulatedTouch)
        {
            m_drawing.Update(_simulatedTouch);
            if (m_trail != null)
            {
                Vector3 trailPosition = Camera.main.ScreenToWorldPoint(_simulatedTouch.Position);
                trailPosition.z = -1;
                m_trail.transform.position = trailPosition;  //move trail
            }
        }

        #endregion

        #region EventHandlers

        private void HandleOnResetDrawing()
        {
            Destroy(m_trail);
			Destroy(m_target);
        }
        
        private void HandleOnStartRound()
        {
            StartNewRound();
        }

        private void HandleOnRoundDone()
        {
            StartNewRound();
        }

        #endregion

        #region Actions

        private void StartNewRound()
        {
            m_template = ShapeTemplates.GetRandomTemplate();
            ShapePreview.Find().CreatePreview(m_template.PointsNormalized);
        }

        #endregion
        
    }
}
