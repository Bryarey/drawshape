﻿using UnityEngine;
using System.Collections;

namespace DrawShape
{

	[System.Serializable]
	public struct ShapePoint
	{
		public Vector2 position;
		public float angle;
		public float distanceBefore;
		public float distanceAfter;
	}
}