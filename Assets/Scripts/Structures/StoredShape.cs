﻿using UnityEngine;
using System;

namespace DrawShape
{
    /// <summary>
    /// This struct used to create and store shape`s corners.
    /// </summary>
    [Serializable]
    public struct StoredShape
    {
        public Vector2[] m_corners;

        public Vector2[] Corners    { get { return m_corners; }  set { m_corners = value; } }
    }
}
