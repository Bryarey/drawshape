﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


namespace DrawShape
{
    /// <summary>
    /// Is a MonoBehaviour, attached to Preview GameObject. It builds a mesh from ShapePoints, and locates it in game window.
    /// </summary>
    [RequireComponent(typeof(MeshRenderer))]
    [RequireComponent(typeof(MeshFilter))]
    public class ShapePreview : MonoBehaviour
    {
        #region PrivateMembers

        private static ShapePreview m_instance; 
        private MeshRenderer m_meshRenderer;    
        private MeshFilter m_meshFilter;
        private float m_scale = 1.0f;

        [SerializeField] private Transform m_pivotObject;

        #endregion

        #region MonoBehaviourMessages
        private void Awake()
        {
            m_meshFilter = gameObject.GetComponent<MeshFilter>();
            m_instance = this;
        }

        private void Start()
        {
            SetInitialPosition();
        }

        #endregion

        /// <summary>
        /// Locate preview`s initial position
        /// </summary>
        private void SetInitialPosition()
        {
            Vector3 newPosition = Camera.main.transform.position;
            newPosition.z = 0;
            float aspect = (float)Screen.width / (float)Screen.height;
            float halfCameraWidth = Camera.main.orthographicSize * aspect;

            m_scale = halfCameraWidth / 2;
        }

        public static ShapePreview Find()
        {
            if (m_instance == null)
            {
                Debug.LogError("ShapePreview isn`t exists on current scene!");
                //TODO: maybe add there preview autocreation later
            }
            return m_instance;
        }

        #region BuildMeshFunctionality

        /// <summary>
        /// Invokes a chain of methods to create preview mesh from given list of shape points.
        /// </summary>
        /// <param name="shapePoints">List of shape points</param>
        public void CreatePreview(List<ShapePoint> shapePoints)
        {
            //Extract positions:
            List<Vector2> verticesList = new List<Vector2>();
            for (int i = 0; i < shapePoints.Count; i++)
            {
                verticesList.Add(shapePoints[i].position * m_scale);
            }

            //Load vertices to builder:
            LoadVertices(verticesList);
        }

        /// <summary>
        /// Loads list of vertices to mesh builder
        /// </summary>
        /// <param name="verticesList"> List of vertices</param>
        private void LoadVertices(List<Vector2> verticesList)
        {
            Vector2[] vertices2D = new Vector2[verticesList.Count];
            verticesList.CopyTo(vertices2D);
            BuildMesh(vertices2D);
        }

        /// <summary>
        /// Builds a triangle mesh based on given vertices.
        /// </summary>
        /// <param name="vertices2D">Array of vertices</param>
        private void BuildMesh(Vector2[] vertices2D)
        {
            /// Next code taken from Unity WIKI without any modifications:
            /// http://wiki.unity3d.com/index.php?title=Triangulator

            // Use the triangulator to get indices for creating triangles
            Triangulator tr = new Triangulator(vertices2D);
            int[] indices = tr.Triangulate();

            // Create the Vector3 vertices
            Vector3[] vertices = new Vector3[vertices2D.Length];
            for (int i = 0; i < vertices.Length; i++)
            {
                vertices[i] = new Vector3(vertices2D[i].x, vertices2D[i].y, 0);
            }

            // Create the mesh
            Mesh msh = new Mesh();
            msh.vertices = vertices;
            msh.triangles = indices;
            msh.RecalculateNormals();
            msh.RecalculateBounds();

            m_meshFilter.mesh = msh;

            //End of wiki`s code

            AlignToPivot(msh);
        }

        /// <summary>
        /// Align created mesh to pivot
        /// </summary>
        /// <param name="mesh">Mesh</param>
        private void AlignToPivot (Mesh mesh)
        {
            transform.position = m_pivotObject.position;
            Vector3 newPosition = transform.position;
            newPosition.x -= mesh.bounds.extents.x;
            newPosition.y -= mesh.bounds.extents.y;
            transform.position = newPosition;
        }

        #endregion

    }
}