﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace DrawShape
{
	public enum PreDefinedShapes {Square, IsoscelesTriangle, RightTriangle, Rhombus, HorizontalRectangle, VerticalRectangle, FourPointedStar, FivePointedStar, SixPointedStar}

    /// <summary>
    /// This class deals with shape templates (what user should draw).
    /// There is hardcoded pre-defined templates, and loaded from XML file templates. 
    /// To create new templates, use ShapesEditor class.
    /// Class ShapesStore used to load templates from HDD.
    /// </summary>
	public static class ShapeTemplates
	{
        #region PrivateMembers

        private static Dictionary < string, Func<Shape> > m_preDefinedTemplates; // this is for shapes, defined in code
        private static List<Shape> m_allTemplates;      //this is for both pre-defined and XML-stored shapes.
        private static int m_previousShapeIndex = -1;   //used to prevent repetitions at random shape get function.

        #endregion

        /// <summary>
        /// Get a random template.
        /// </summary>
        /// <returns>Shape template</returns>
        public static Shape GetRandomTemplate()
		{
            //check initialization:
            if (m_allTemplates == null)
            {
                InitializeAllTemplates();
            }

            int index = UnityEngine.Random.Range(0, m_allTemplates.Count - 1);

            //Prevent repetitions:
            while (index == m_previousShapeIndex)  //while random value equals previous value 
            {
                index = UnityEngine.Random.Range(0, m_allTemplates.Count - 1);  //generate new random value
            }
            m_previousShapeIndex = index;

            return m_allTemplates[index];   //You can set there a particular index to test any particular shape
        }

        /// <summary>
        /// Initialize Pre-Defined templates and XML-templates.
        /// </summary>
        private static void InitializeAllTemplates()
        {
            // initialize List:
            m_allTemplates = new List<Shape>();

            //Add pre-defined templates:
            Array A = Enum.GetValues(typeof(PreDefinedShapes));
            for (int i = 0; i < A.Length; i++)
            {
                m_allTemplates.Add(GetShapeTemplate((PreDefinedShapes)i));
            }

            //Add external templates:
            ShapesStore shapesStore = new ShapesStore();    // shapes loaded from XML automatically at ShapesStore creation

            // In ShapesStore stored only array of arrays of corner points, so we need to make new shapes from all of these points:
            for (int j = 0; j < shapesStore.Shapes.Count; j++)
            {
                Shape newShape = new Shape(shapesStore.Shapes[j].Corners);
                m_allTemplates.Add(newShape);
            }
        }

        #region Pre-Defined_Templates_Management

        /// <summary>
        /// Initialization of pre-defined shapes method`s dictionary
        /// </summary>
        private static void InitializeDictionary()
		{
			m_preDefinedTemplates = new Dictionary< string, Func<Shape> >();
			m_preDefinedTemplates.Add("Square", () => ShapeTemplates.Square() );
			m_preDefinedTemplates.Add("IsoscelesTriangle", () => ShapeTemplates.IsoscelesTriangle() );
			m_preDefinedTemplates.Add("RightTriangle", () => ShapeTemplates.RightTriangle() );
			m_preDefinedTemplates.Add("Rhombus", () => ShapeTemplates.Rhombus() );
			m_preDefinedTemplates.Add("HorizontalRectangle", () => ShapeTemplates.HorizontalRectangle() );
			m_preDefinedTemplates.Add("VerticalRectangle", () => ShapeTemplates.VerticalRectangle() );
			m_preDefinedTemplates.Add("FourPointedStar", () => ShapeTemplates.FourPointedStar() );
			m_preDefinedTemplates.Add("FivePointedStar", () => ShapeTemplates.FivePointedStar() );
			m_preDefinedTemplates.Add("SixPointedStar", () => ShapeTemplates.SixPointedStar() );
		}

		public static Shape GetShapeTemplate(PreDefinedShapes template)
		{
			if (m_preDefinedTemplates == null || m_preDefinedTemplates.Count == 0){
				InitializeDictionary();
			}
          
            Func<Shape> templateCreatorMethod = m_preDefinedTemplates[template.ToString()];
            return templateCreatorMethod();
		}

		private static Shape Square()
		{
			List<CornerPoint> corners = new List<CornerPoint>();
			corners.Add(new CornerPoint(new Vector2(0, 0)));
			corners.Add(new CornerPoint(new Vector2(100, 0)));
			corners.Add(new CornerPoint(new Vector2(100, 100)));
			corners.Add(new CornerPoint(new Vector2(0, 100)));
			Shape template = new Shape(corners);
			return template;
		}

		private static Shape IsoscelesTriangle()
		{
			List<CornerPoint> corners = new List<CornerPoint>();
			corners.Add(new CornerPoint(new Vector2(0, 100)));
			corners.Add(new CornerPoint(new Vector2(100, 100)));
			corners.Add(new CornerPoint(new Vector2(50, 0)));
			Shape template = new Shape(corners);
			return template;
		}

		private static Shape RightTriangle()
		{
			List<CornerPoint> corners = new List<CornerPoint>();
			corners.Add(new CornerPoint(new Vector2(0, 0)));
			corners.Add(new CornerPoint(new Vector2(100, 0)));
			corners.Add(new CornerPoint(new Vector2(0, 100)));
			Shape template = new Shape(corners);
			return template;
		}

		private static Shape Rhombus()
		{
			List<CornerPoint> corners = new List<CornerPoint>();
			corners.Add(new CornerPoint(new Vector2(50, 100)));
			corners.Add(new CornerPoint(new Vector2(100, 50)));
			corners.Add(new CornerPoint(new Vector2(50, 0)));
			corners.Add(new CornerPoint(new Vector2(0, 50)));
			Shape template = new Shape(corners);
			return template;
		}

		private static Shape HorizontalRectangle()
		{
			List<CornerPoint> corners = new List<CornerPoint>();
			corners.Add(new CornerPoint(new Vector2(0, 0)));
			corners.Add(new CornerPoint(new Vector2(200, 0)));
			corners.Add(new CornerPoint(new Vector2(200, 100)));
			corners.Add(new CornerPoint(new Vector2(0, 100)));
			Shape template = new Shape(corners);
			return template;
		}

		private static Shape VerticalRectangle()
		{
			List<CornerPoint> corners = new List<CornerPoint>();
			corners.Add(new CornerPoint(new Vector2(0, 0)));
			corners.Add(new CornerPoint(new Vector2(50, 0)));
			corners.Add(new CornerPoint(new Vector2(50, 100)));
			corners.Add(new CornerPoint(new Vector2(0, 100)));
			Shape template = new Shape(corners);
			return template;
		}

		private static Shape FourPointedStar()
		{
			List<CornerPoint> corners = new List<CornerPoint>();
			corners.Add(new CornerPoint(new Vector2(50, 100)));
			corners.Add(new CornerPoint(new Vector2(60, 60)));
			corners.Add(new CornerPoint(new Vector2(100, 50)));
			corners.Add(new CornerPoint(new Vector2(60, 40)));
			corners.Add(new CornerPoint(new Vector2(50, 0)));
			corners.Add(new CornerPoint(new Vector2(40, 40)));
			corners.Add(new CornerPoint(new Vector2(0, 50)));
			corners.Add(new CornerPoint(new Vector2(40, 60)));
			Shape template = new Shape(corners);
			return template;
		}

		private static Shape FivePointedStar()
		{
			List<CornerPoint> corners = new List<CornerPoint>();
			corners.Add(new CornerPoint(new Vector2(50, 100)));
			corners.Add(new CornerPoint(new Vector2(62, 65)));
			corners.Add(new CornerPoint(new Vector2(100, 60)));
			corners.Add(new CornerPoint(new Vector2(70, 40)));
			corners.Add(new CornerPoint(new Vector2(80, 0)));
			corners.Add(new CornerPoint(new Vector2(50, 30)));
			corners.Add(new CornerPoint(new Vector2(20, 0)));
			corners.Add(new CornerPoint(new Vector2(30, 40)));
			corners.Add(new CornerPoint(new Vector2(0, 60)));
            corners.Add(new CornerPoint(new Vector2(38, 65)));
            Shape template = new Shape(corners);
			return template;
		}

		private static Shape SixPointedStar()
		{
			List<CornerPoint> corners = new List<CornerPoint>();
			corners.Add(new CornerPoint(new Vector2(50, 100)));
            corners.Add(new CornerPoint(new Vector2(70, 75)));
            corners.Add(new CornerPoint(new Vector2(100, 75)));
			corners.Add(new CornerPoint(new Vector2(80, 50)));
			corners.Add(new CornerPoint(new Vector2(100, 25)));
            corners.Add(new CornerPoint(new Vector2(70, 25)));
            corners.Add(new CornerPoint(new Vector2(50, 0)));
            corners.Add(new CornerPoint(new Vector2(30, 25)));
            corners.Add(new CornerPoint(new Vector2(0, 25)));
			corners.Add(new CornerPoint(new Vector2(20, 50)));
			corners.Add(new CornerPoint(new Vector2(0, 75)));
            corners.Add(new CornerPoint(new Vector2(30, 75)));

            Shape template = new Shape(corners);
			return template;
		}

        #endregion
    }
}
