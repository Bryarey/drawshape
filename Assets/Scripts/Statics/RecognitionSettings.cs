﻿using UnityEngine;
using System.Collections;

namespace DrawShape
{
    /// <summary>
    /// This is a store for recognition settings and parameters. Be careful to change it. 
    /// TODO: Implement mechanism to change settings depending to current difficulty level.
    /// </summary>
    public static class RecognitionSettings
    {
        internal static readonly float angularTreshold = 15f;   //minimal corner angle - used to filter too small corners
        internal static readonly float normalizedDistanceTreshold = 0.1f;   //minimal segment length - used to filter too small segments (trash points)

        internal static readonly int minPathPointsNumberForCheckCorners = 3;      //number of points before current potential corner point to analize direction

        internal static readonly int motionTreshold = 1;      //minimal vector size for comparing directions

        internal static readonly int minDistanceBtwPathPoints = 5; // min distance between corners in pixels

        internal static readonly int fingerWidth = 30;  // Max distance btw start drawing point and current pointer position to close drawing line.

        internal static readonly int weldingTreshold = 20;   //treshold for welding corner points of drawing. 20 means that drawing width (or height) will be divided to 20, and the result is a welding distance

        internal static readonly float normalizedTreshold = 0.2f; // Accepted difference from template. Used during shape`s comparsion. 
    }
}
