﻿using UnityEngine;
using UnityEditor;

namespace DrawShape
{
    /// <summary>
    /// Custom Editor class for ShapesEditor.
    /// Implements inspector buttons (Save shape and Reset shape) and shape handles in scene view.
    /// </summary>
    [CustomEditor(typeof (ShapesEditor))]
    public class ShapesEditorInspector : Editor
    {
        ShapesEditor m_shapesEditor = null;

        //Property needed for assign target as valid ShapesEditor object
        public ShapesEditor ShapesEditor {
            get
            {
                if (m_shapesEditor == null)
                {
                    m_shapesEditor = target as ShapesEditor;
                }
                return m_shapesEditor;
            }

            set
            {
                if (m_shapesEditor == null)
                {
                    m_shapesEditor = target as ShapesEditor;
                }
                m_shapesEditor = value;
            }
        }

        /// <summary>
        /// Draw default inspector, help text and buttons.
        /// </summary>
        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            
            //Some help:
            EditorGUILayout.HelpBox("Press CTRL or CMD to snap points ", MessageType.Info);
            EditorGUILayout.HelpBox("Set numper of points as array size. Drag points in scene view to make some shape.", MessageType.Info);

            DrawDefaultInspector();

            //Buttons:
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Save shape"))
            {
                ShapesEditor.SaveShape();
            }
            if (GUILayout.Button("Reset shape"))
            {
                ShapesEditor.ResetShape();
            }
            EditorGUILayout.EndHorizontal();
        }

        /// <summary>
        /// Draw handles for shape points.
        /// </summary>
        private void OnSceneGUI()
        {
            for (int i = 0; i < ShapesEditor.CurrentCorners.Count; i++)
            {
                ShapesEditor.CurrentCorners[i] = Handles.FreeMoveHandle(ShapesEditor.CurrentCorners[i], Quaternion.identity, 0.6f, Vector3.one, Handles.CubeCap);
            }

            if (GUI.changed)
            {
                EditorUtility.SetDirty(target);
            }
        }
    }
}

