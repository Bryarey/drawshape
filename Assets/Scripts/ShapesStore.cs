﻿using UnityEngine;
using System.Xml.Serialization;
using System.IO;
using System;
using System.Collections.Generic;

namespace DrawShape
{
    /// <summary>
    /// ShapeStore performs Save/Load operations with Shapes.
    /// Shapes stored as arrays of points, connected by array indexes.
    /// </summary>
    [Serializable]
    public class ShapesStore
    {
        private const string FILE_EXTENSION = ".xml";
        private const string FILE_NAME = "shapes_data";
        private const string PATH = "Assets/Resources/";

        private List<StoredShape> m_shapes;
        public List<StoredShape> Shapes { get { return m_shapes; } set { m_shapes = value; } }

        /// <summary>
        /// Constructor: automatically load shape at instance creation.
        /// </summary>
        public ShapesStore ()
        {
            LoadShapes();
        }

        /// <summary>
        /// Add new shape to array. Transforms list of points to <StoredShape> structure, then add it.
        /// </summary>
        /// <param name="shape">List of shape points.</param>
        public void AddShape(List<Vector2> shape)
        {
            StoredShape newShape = new StoredShape();
            newShape.Corners = new Vector2[shape.Count];
            shape.CopyTo(newShape.Corners);
            Shapes.Add(newShape);
        }

        /// <summary>
        /// Load shapes_data.xml from resources, and deserialize it. If there is no such file, just create new empty shapes container.
        /// </summary>
        public void LoadShapes()
        {
            string data = "";
            TextAsset ta = Resources.Load(FILE_NAME) as TextAsset;  //Try load textAsset...
            if (ta != null)
            { 
                // loading done - deserialize content:
                data = ta.text;
                var serializer = new XmlSerializer(typeof(List<StoredShape>));  
                m_shapes = serializer.Deserialize(new StringReader(data)) as List<StoredShape>; 
            } else
            {
                //Can not load from file (does not exists?) - create empty container:
                Shapes = new List<StoredShape>();
            }
        }

        /// <summary>
        /// Save shape - this method used only by ShapesEditor class, and only in editor mode. Stores shapesarray into "Resources" folder.
        /// </summary>
        public void SaveShapes()
        {
#if UNITY_EDITOR
            //Serialize:
            var serializer = new XmlSerializer(Shapes.GetType());
            var stringwriter = new StringWriter();
            serializer.Serialize(stringwriter, Shapes);
            string data = stringwriter.ToString();

            //Save:
            string path = string.Concat(PATH, FILE_NAME, FILE_EXTENSION);
            using (FileStream fs = new FileStream(path, FileMode.Create))
            {
                using (StreamWriter writer = new StreamWriter(fs))
                {
                    writer.Write(data);
                }
            }

            //Refresh assets:
            UnityEditor.AssetDatabase.Refresh();
#endif

        }
    }
}
