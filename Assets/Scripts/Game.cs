﻿using UnityEngine;
using System.Collections;

namespace DrawShape
{

    #region Delegates

    public delegate void OnStartRoundHandler();
    public delegate void OnResetDrawingHandler();
    public delegate void OnRoundDoneHandler();
    public delegate void OnGameOverHandler();

    #endregion

    /// <summary>
    /// THis is main game events manager. 
    /// Also initial game settings are here.
    /// </summary>
    public class Game : MonoBehaviour
    {

        #region Events

        public event OnStartRoundHandler OnStartRound;
        public event OnResetDrawingHandler OnResetDrawing;
        public event OnRoundDoneHandler OnRoundDone;
        public event OnGameOverHandler OnGameOver;

        #endregion

        #region PrivateMembers

        private float m_roundLength = 60.0f;            //Current round length
        private float m_roundTimer = 60.0f;             // Current round timer
        private float m_roundLengthMultipler = 0.95f;   //multipler to decrease round time every next round
        private int m_currentRound = 1;                 //Current round number

        private bool m_isPlaying = true;                //pause toggle

        private static Game m_instance;

        #endregion

        #region PublicProperties

        public float RoundLength { get { return m_roundLength; } set { m_roundLength = value; } }
        public float RoundTimer  { get { return m_roundTimer;  } set { m_roundTimer = value;  } }
        public int CurrentRound { get { return m_currentRound; } set { m_currentRound = value; } }

        public bool IsPlaying
        {
            get
            {
                return m_isPlaying;
            }

            set
            {
                m_isPlaying = value;
            }
        }

        #endregion

        public static Game Find()
        {
            return m_instance;
        }

        #region MonoBehaviourMessages

        private void Awake()
        {
            m_instance = this;
        }

        void Update()
        {
            if (IsPlaying)
            {
                UpdateTimer();
            }
        }

        #endregion

        /// <summary>
        /// Update round timer, if time runs out - invoke GameOver()
        /// </summary>
        private void UpdateTimer()
        {
            if (RoundTimer > 0)
            {
                RoundTimer -= Time.deltaTime;
            }
            else
            {
                GameOver();
            }
        }

        #region Actions

        private void GameOver()
        {
            IsPlaying = false;
            OnGameOver();
        }

        /// <summary>
        /// Raise event OnStartRound, decrease round`s length, increase completed rounds count (score).
        /// </summary>
        public void WinRound()
        {
            OnStartRound();

            RoundLength *= m_roundLengthMultipler;
            RoundTimer = RoundLength;

            CurrentRound++;
        }

        /// <summary>
        /// Raise event OnResetDrawing
        /// </summary>
        public void ResetDrawing()
        {
            OnResetDrawing();
        }

        #endregion


    }
}