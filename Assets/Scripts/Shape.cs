
using UnityEngine;
using System.Collections.Generic;
using System;

namespace DrawShape
{
    /// <summary>
	/// Shape is an analized drawing (or template). Can be compared with another Shape (template).
	/// </summary>
	[Serializable]
	public class Shape
	{
        #region PrivateMembers

        private List<ShapePoint> m_points = new List<ShapePoint>();
        [SerializeField]
        private List<ShapePoint> m_pointsNormalized = new List<ShapePoint>();
        private List<ShapePoint> m_scaleAndOrderIndependentPoints = new List<ShapePoint>();

        #endregion

        //TODO: Somewhy, next fields generates an error when encapsulated, so I decided to leave them public:
        public Bounds m_bounds;
		public float m_aspect = 1.0f;

        #region PublicProperties
        public List<ShapePoint> Points
        {
            get
            {
                return m_points;
            }

            set
            {
                m_points = value;
            }
        }

        public List<ShapePoint> PointsNormalized
        {
            get
            {
                return m_pointsNormalized;
            }

            set
            {
                m_pointsNormalized = value;
            }
        }

        public List<ShapePoint> ScaleAndOrderIndependentPoints
        {
            get
            {
                return m_scaleAndOrderIndependentPoints;
            }

            set
            {
                m_scaleAndOrderIndependentPoints = value;
            }
        }

        #endregion

        #region Constructors

        public Shape ()
		{
		}

        /// <summary>
        /// Constructor for creating shapes from list of <CornerPoint>. Used to create shape from drawing.
        /// </summary>
        /// <param name="corners"></param>
		public Shape (List<CornerPoint> corners)
		{
            if (corners.Count > 2)
            {
                Vector2 previousPoint = corners[corners.Count - 1].position;
                Vector2 nextPoint = corners[1].position;
                for (int i = 0; i < corners.Count; i++)
                {
                    ShapePoint newPoint = new ShapePoint();
                    newPoint.position = corners[i].position;
                    ProcessCornerPoint(previousPoint, nextPoint, newPoint);

                    //Set up next iteration:
                    previousPoint = corners[i].position;
                    int nextPointIndex = i + 2;
                    if (nextPointIndex < corners.Count)
                    {
                        nextPoint = corners[i + 2].position;
                    }
                    else {
                        nextPoint = corners[0].position;
                    }

                }

                PrepareToComparsion();
            }
		}

        /// <summary>
        /// Constructor used for creating shapes from XML - only groups of Vector2 positions stored. <ShapePoint> object will be created from every Vector2 value.
        /// </summary>
        /// <param name="corners">Array of corner points</param>
        public Shape(Vector2[] corners)
        {
            Vector2 previousPoint = corners[corners.Length - 1];
            Vector2 nextPoint = corners[1];
            for (int i = 0; i < corners.Length; i++)
            {
                ShapePoint newPoint = new ShapePoint();
                newPoint.position = corners[i];
                ProcessCornerPoint(previousPoint, nextPoint, newPoint);

                //Set up next iteration:
                previousPoint = corners[i];
                int nextPointIndex = i + 2;
                if (nextPointIndex < corners.Length)
                {
                    nextPoint = corners[i + 2];
                }
                else {
                    nextPoint = corners[0];
                }
            }
            PrepareToComparsion();
        }

        #endregion

        #region ProcessingPoints

        /// <summary>
        /// Sets corner point`s parameters - angle and distance, and then adds point to array.
        /// </summary>
        /// <param name="previousPoint"></param>
        /// <param name="nextPoint"></param>
        /// <param name="newPoint"></param>
        private void ProcessCornerPoint(Vector2 previousPoint, Vector2 nextPoint, ShapePoint newPoint)
        {
            //Define previous and next points:
            Vector2 previous = newPoint.position - previousPoint;
            Vector2 next = nextPoint - newPoint.position;

            //Set angle:
            newPoint.angle = Vector2.Angle(previous, next);
            newPoint.angle = 180f - newPoint.angle;
            newPoint.angle = newPoint.angle * Mathf.Sign(Vector3.Cross(previous, next).z);

            //Set distances:
            newPoint.distanceBefore = Vector2.Distance(previousPoint, newPoint.position);
            newPoint.distanceAfter = Vector2.Distance(newPoint.position, nextPoint);

            Points.Add(newPoint);
        }

        /// <summary>
        /// Prepare points to comparsion: define bounds, aspect and normalized positions.
        /// </summary>
        private void PrepareToComparsion ()
		{
			m_bounds = new Bounds (Points [0].position, Vector3.zero);
			for (int i = 0; i < Points.Count; i++) {
				m_bounds.Encapsulate (Points [i].position);
			}
			m_aspect = m_bounds.size.x / m_bounds.size.y;

			NormalizePoints ();
		}

        /// <summary>
        /// Fills <pointsNormalized> list with normalized coordinates. Maximum size (width or height) taken as a base for normalisation.
        /// </summary>
		private void NormalizePoints ()
		{
            //Prepare normalized points List
			PointsNormalized = new List<ShapePoint> ();
			PointsNormalized.AddRange (Points);

            float normalizationBase = Mathf.Max(m_bounds.size.x, m_bounds.size.y);  // this is a size, wich will be taken as 1.0 value during normalization;

            for (int j = 0; j < Points.Count; j++) {
                // Calculate normalized position
                Vector2 normalizedPos = new Vector2((PointsNormalized[j].position.x - m_bounds.min.x) / normalizationBase, (PointsNormalized[j].position.y - m_bounds.min.y) / normalizationBase);
                float normalizedDistanceBefore = PointsNormalized[j].distanceBefore / normalizationBase;
                float normalizedDistanceAfter = PointsNormalized[j].distanceAfter/ normalizationBase;

                // move point to normalized position
                ShapePoint currentPoint = PointsNormalized [j];
				currentPoint.position = normalizedPos;
                currentPoint.distanceBefore = normalizedDistanceBefore;
                currentPoint.distanceAfter = normalizedDistanceAfter;
                PointsNormalized [j] = currentPoint;
			}

            FilterTrashPoints();

        }

        /// <summary>
        /// Find and remove all points, located close to another. 
        /// </summary>
        private void FilterTrashPoints()
        {
            //initialize
            List<ShapePoint> filteredNormalizedPoints = new List<ShapePoint>();

            //Find non-trash points:
            for (int i = 0; i < PointsNormalized.Count; i++)
            {
                if (Mathf.Abs(PointsNormalized[i].angle) > RecognitionSettings.angularTreshold && PointsNormalized[i].distanceAfter > RecognitionSettings.normalizedDistanceTreshold)
                {
                    //add to temp list:
                    filteredNormalizedPoints.Add(PointsNormalized[i]);
                }
            }

            ///Re-create PointsNormalized list from temp list (trash points are excluded):
            if (filteredNormalizedPoints.Count >= 3)
            {
                PointsNormalized = new List<ShapePoint>(filteredNormalizedPoints);
            }
        }

        #endregion

        #region Comparsion

        /// <summary>
        /// Compare shape with given template. Points normalized positions used to compare.
        /// </summary>
        /// <param name="template"> Shape template.</param>
        /// <returns> true if shape approximatelly equals template.</returns>
        public bool Compare (Shape template)
		{
            // Define drawing direction and template point, nearest to drawing`s first point:
            int firstTemplatePointIndex = 0;
            int drawingDirection = DrawingDirection(template, out firstTemplatePointIndex); //DrawingDirection can be 1 if equals template or -1 if opposite
            if (drawingDirection == 0)
            {
                //Can not define drawing direction 
                return false;
            }

            int currentTemplateIndex = firstTemplatePointIndex;

            //Iterate rest of drawing`s points:
            for (int i = 1; i < PointsNormalized.Count; i++)
            {
                //if there is a direction change (real corner, not just point on a straight line)
                if (Mathf.Abs(PointsNormalized[i].angle) > RecognitionSettings.angularTreshold)
                {
                    //Get current template point index
                    currentTemplateIndex = CycleArrayIndex(currentTemplateIndex, drawingDirection, template.PointsNormalized.Count);

                    // current point`s nearest template index:
                    int nearestTemplatePointIndex = FindNearestPointIndex(template.PointsNormalized, PointsNormalized[i].position);

                    if (nearestTemplatePointIndex == -1)
                    {
                        //drawing point is too far from any template point
                        return false;
                    } 
                    if (nearestTemplatePointIndex != currentTemplateIndex)
                    {
                        //nearest template point is not match next template point (wrong template point)
                        return false;
                    }
                }

            }

            //All checks done
            return true;
		}

        /// <summary>
        /// Compare shape with given template. Angles and distances used to compare. It is now independent from figure angle.
        /// </summary>
        /// <param name="template"> Shape template.</param>
        /// <returns> true if shape approximatelly equals template.</returns>
        public bool CompareByAnglesAndDistances (Shape template)
        {
            if (PointsNormalized.Count < 3)
            {
                //There is No points to compare - fail
                return false;
            }

            // Define drawing direction and template point, nearest to drawing`s first point:
            int firstTemplatePointIndex = 0;
            int drawingDirection = DrawingDirection(template, out firstTemplatePointIndex); //DrawingDirection can be 1 if equals template or -1 if opposite
            if (drawingDirection == 0)
            {
                //Can not define drawing direction 
                return false;
            }

            int currentTemplateIndex = firstTemplatePointIndex;

            //Iterate rest of drawing`s points:
            for (int i = 1; i < PointsNormalized.Count; i++)
            {
                //if there is a direction change (real corner, not just point on a straight line)
                if (Mathf.Abs(PointsNormalized[i].angle) > RecognitionSettings.angularTreshold)
                {
                    //Get current template point index
                    currentTemplateIndex = CycleArrayIndex(currentTemplateIndex, drawingDirection, template.PointsNormalized.Count);

                    if (!MeasureAngleAndDistance(template, drawingDirection, currentTemplateIndex, i))
                    {
                        return false;
                    }
                }

            }

            //All checks done
            return true;
        }

        /// <summary>
        /// Mesures distance and angular difference btw shape and template.
        /// </summary>
        /// <param name="template">template shape</param>
        /// <param name="drawingDirection">drawing direction (-1 or 1)</param>
        /// <param name="currentTemplateIndex">Index of current template point</param>
        /// <param name="index">Index of current shape point</param>
        /// <returns>returns true when differences does not exceeds tresholds (shape approximatelly equals template).</returns>
        private bool MeasureAngleAndDistance(Shape template, int drawingDirection, int currentTemplateIndex, int index)
        {
            //Calculate vectors:
            Vector2 shapeSegmentBefore = PointsNormalized[index].position - PointsNormalized[index - 1].position;
            Vector2 shapeSegmentAfter = PointsNormalized[CycleArrayIndex(index, 1, PointsNormalized.Count)].position - PointsNormalized[index].position;
            Vector2 templateSegmentBefore = template.PointsNormalized[currentTemplateIndex].position - template.PointsNormalized[CycleArrayIndex(currentTemplateIndex, -1 * drawingDirection, template.PointsNormalized.Count)].position;
            Vector2 templateSegmentAfter = template.PointsNormalized[CycleArrayIndex(currentTemplateIndex, drawingDirection, template.PointsNormalized.Count)].position - template.PointsNormalized[currentTemplateIndex].position;

            if (MeasureAngles(shapeSegmentBefore, shapeSegmentAfter, templateSegmentBefore, templateSegmentAfter))
            {
                // Angle does not exceeds treshold
                if (MeasureDistances(shapeSegmentAfter, templateSegmentAfter))
                {
                    return true;   // Distance does not exceeds treshold
                }
            }
            return false;
        }

        /// <summary>
        /// Measures that distance btw two shape points are approximatelly equals distance btw two template points.
        /// </summary>
        /// <param name="shapeSegmentAfter">Vector from first to second point in shape</param>
        /// <param name="templateSegmentAfter">Vector from first to second point in template</param>
        /// <returns>returns true if distance`s difference does not exceeds treshold (taken from RecognitionSettings)</returns>
        private static bool MeasureDistances(Vector2 shapeSegmentAfter, Vector2 templateSegmentAfter)
        {
            float shapeDistance = shapeSegmentAfter.magnitude;
            float templateDistance = templateSegmentAfter.magnitude;

            if (shapeDistance < templateDistance - (templateDistance / 3) || shapeDistance > templateDistance + (templateDistance / 3))
            {
                return false;   // Distance exceeds treshold - fail
            }
            return true;
        }

        /// <summary>
        /// Compare angles btw two shape segments and two template segments.
        /// </summary>
        /// <param name="shapeSegmentBefore">first shape segment</param>
        /// <param name="shapeSegmentAfter">second shape segment</param>
        /// <param name="templateSegmentBefore">first template segment</param>
        /// <param name="templateSegmentAfter">second template segment</param>
        /// <returns>returns true, when angle`s difference does not exceeds treshold (taken from RecognitionSettings)</returns>
        private static bool MeasureAngles(Vector2 shapeSegmentBefore, Vector2 shapeSegmentAfter, Vector2 templateSegmentBefore, Vector2 templateSegmentAfter)
        {
            float shapeAngle = Vector2.Angle(shapeSegmentBefore, shapeSegmentAfter);
            float templateAngle = Vector2.Angle(templateSegmentBefore, templateSegmentAfter);

            if (shapeAngle < templateAngle - (templateAngle / 2) || shapeAngle > templateAngle + (templateAngle / 2))
            {
                return false;   // Angle exceeds treshold - fail
            }

            return true;
        }

        #endregion

        #region Utility

        /// <summary>
        /// Set direction as 1 or -1.
        /// </summary>
        /// <param name="arrayIndex">current index</param>
        /// <param name="direction"> Must be 1 (forward) or -1 (backward)</param>
        /// <param name="arrayLength">Length of cycled array</param>
        /// <returns> next or previous index depending from direction parameter</returns>
        private int CycleArrayIndex(int arrayIndex, int direction, int arrayLength)
        {
            arrayIndex += direction;
            if (arrayIndex < 0)
            {
                arrayIndex = arrayLength - 1;
            }
            else
            {
                if (arrayIndex > arrayLength - 1)
                {
                    arrayIndex = 0;
                }
            }
            return arrayIndex;
        }

        private bool CycleArrayIndex_Test()
        {
            if (CycleArrayIndex(0, 1, 3) != 1) { return false; }
            if (CycleArrayIndex(0, -1, 3) != 2) { return false; }
            if (CycleArrayIndex(1, 1, 3) != 2) { return false; }
            if (CycleArrayIndex(1, -1, 3) != 0) { return false; }
            if (CycleArrayIndex(2, 1, 3) != 0) { return false; }
            if (CycleArrayIndex(2, -1, 3) != 1) { return false; }
            Debug.Log("CycleArrayIndex_Test DONE!");
            return true;
        }

        /// <summary>
        /// Define drawing direction. Finds two pooints in template, nearest to first two points of drawing, and compares it`s indexes.
        /// </summary>
        /// <param name="template">template to comparsion.</param>
        /// <param name="firstTemplatePointIndex">output - index of template point, nearest to drawing`s first point.</param>
        /// <returns> 1 if drawing direction equals template, and -1 if opposite. If direction can not be defined, returns 0.</returns>
        private int DrawingDirection(Shape template, out int firstTemplatePointIndex)
        {
            int direction = 0;  //wrong direction

            firstTemplatePointIndex = FindNearestPointIndex(template.PointsNormalized, PointsNormalized[0].position);

            if (firstTemplatePointIndex == -1)
            {
                //Can not locate start template point
                return direction;
            }
          
            int secondIndex = FindNearestPointIndex(template.PointsNormalized, PointsNormalized[1].position);
            if (firstTemplatePointIndex == secondIndex - 1)
            {
                direction = 1;
            }
            if (firstTemplatePointIndex == template.PointsNormalized.Count - 1 && secondIndex == 0)
            {
                direction = 1;
            }
            if (firstTemplatePointIndex == secondIndex + 1)
            {
                direction = -1;
            }
            if (firstTemplatePointIndex == 0 && secondIndex == template.PointsNormalized.Count - 1)
            {
                direction = -1;
            }
            return direction;
        }

        /// <summary>
        /// Find index of template point, nearest to given Vector2 position. Takes RecognitionSettings.normalizedTreshold to attention as max distance to point.
        /// </summary>
        /// <param name="templatePoints"> array of template points</param>
        /// <param name="point">Position</param>
        /// <returns>index of nearest template point. if nearest point not founded, returns -1.</returns>
        private int FindNearestPointIndex(List <ShapePoint> templatePoints, Vector2 point)
        {
            float nearestDistance = RecognitionSettings.normalizedTreshold; 
            int nearestIndex = -1;
            for (int i = 0; i < templatePoints.Count; i++)
            {
                float currentDistance = Vector2.Distance(templatePoints[i].position, point);
                if (nearestDistance > currentDistance)
                {
                    nearestDistance = currentDistance;
                    nearestIndex = i;
                }
            }
            return nearestIndex;
        }
    #endregion
    }
}


