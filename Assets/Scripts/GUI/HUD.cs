﻿using UnityEngine;
using UnityEngine.UI;


namespace DrawShape
{
    /// <summary>
    /// This is GameScene Canvas script. Controls gameplay UI elements.
    /// </summary>
    public class HUD : MonoBehaviour
    {
        #region PrivateMembers

        [SerializeField]
        private Slider m_timeSlider;    //assign time slider here

        [SerializeField]
        private Text m_round;   //assign Text object to show completed rounds count (score).

        [SerializeField]
        private CanvasGroup m_gameOverWindow; //Assign CanvasGroup component of game over window.

        #endregion

        #region MonoBehaviourMessages

        //subscribe events
        private void Start()
        {
            Game.Find().OnStartRound += HandleOnStartRound;
            Game.Find().OnResetDrawing += HandleOnInterruptDrawing;
            Game.Find().OnGameOver += HandleOnGameOver;
        }

        //update HUD elements
        private void Update()
        {
            if (Game.Find() != null)
            {
                m_timeSlider.value = Mathf.InverseLerp(0.0f, Game.Find().RoundLength, Game.Find().RoundTimer);
                m_round.text = Game.Find().CurrentRound.ToString();
            }
        }

        //unsubscribe events
        private void OnDestroy()
        {
            Game.Find().OnStartRound -= HandleOnStartRound;
            Game.Find().OnResetDrawing -= HandleOnInterruptDrawing;
            Game.Find().OnGameOver -= HandleOnGameOver;
        }

        #endregion

        #region Actions

        /// <summary>
        /// Exit to main menu
        /// </summary>
        public void Exit()
        {
            Application.LoadLevel("Menu");
        }

        /// <summary>
        /// Start new game
        /// </summary>
        public void NewGame()
        {
            Application.LoadLevel("Gameplay");
        }

        #endregion

        #region EventHandlers


        private void HandleOnStartRound()
        {
            //TODO: some special effects may be implemented there
        }

        private void HandleOnInterruptDrawing()
        {
            //TODO: some special effects may be implemented there
        }

        private void HandleOnGameOver()
        {
            m_gameOverWindow.alpha = 1.0f;
        }

        #endregion
    }
}