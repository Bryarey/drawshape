﻿using UnityEngine;

namespace DrawShape
{
    /// <summary>
    /// Main menu canvas script.
    /// </summary>
    public class MainMenu : MonoBehaviour
    {
        public void Exit()
        {
            Application.Quit();
        }

        public void Play()
        {
            Application.LoadLevel("Gameplay");
        }

    }
}
