﻿using UnityEngine;
//using UnityEditor;
using System.Collections.Generic;
using System;

namespace DrawShape
{
    /// <summary>
    /// Shapes editor.
    /// THis script executed only in editor. Use it do add new shapes to ShapesStore.
    /// 
    /// Usage:
    /// 1. Drag GameObject ShapesEditor into scene view from Resources/Prefabs folder, and select it.
    /// 2. You will see array "Current Corners" in inspector. Change array size to set number of points you need.
    /// 3. Drag indexed handlers in scene view to make some shape template.
    /// 4. Use Ctrl or Cmd keys to keep handles aligned to grid.
    /// 5. Press button "Save shape" in inspector to save current shape.
    /// 
    /// See ShapesEditorInspector class to review inspector and scene view functionality.
    /// </summary>
    [ExecuteInEditMode]
    [Serializable]
    public class ShapesEditor : MonoBehaviour
    {
        private StoredShape m_currentShape;
        [SerializeField]
        private List<Vector2> m_currentCorners = new List<Vector2>();
        public List<Vector2> CurrentCorners { get { return m_currentCorners; } set { m_currentCorners = value; } }

        /// <summary>
        /// Reset current shape
        /// </summary>
        public void ResetShape()
        {
            CurrentCorners.Clear();
        }

        /// <summary>
        /// Save current shape
        /// </summary>
        public void SaveShape()
        {
            ShapesStore store = new ShapesStore();
            store.AddShape(CurrentCorners);
            store.SaveShapes();
        }

        /// <summary>
        /// Draw connecting lines btw shape points and point`s indexes.
        /// </summary>
        private void OnDrawGizmos()
        {
#if UNITY_EDITOR
            if (m_currentCorners != null && m_currentCorners.Count > 0)
            {
                
                for (int i = 1; i < m_currentCorners.Count; i++)
                {
                    Gizmos.DrawLine(m_currentCorners[i - 1], m_currentCorners[i]);
                    UnityEditor.Handles.Label(m_currentCorners[i - 1], i.ToString());
                }
                Gizmos.DrawLine(m_currentCorners[m_currentCorners.Count - 1], m_currentCorners[0]);
                UnityEditor.Handles.Label(m_currentCorners[m_currentCorners.Count - 1], m_currentCorners.Count.ToString());
             }
#endif
        }
    }
}